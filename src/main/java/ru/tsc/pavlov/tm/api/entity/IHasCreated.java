package ru.tsc.pavlov.tm.api.entity;
import org.jetbrains.annotations.Nullable;
import java.util.Date;

public interface IHasCreated {

    void setCreated(@Nullable Date created);

    @Nullable Date getCreated();
}
