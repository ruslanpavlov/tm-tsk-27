package ru.tsc.pavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    boolean existsByIndex(@NotNull String userId, @NotNull int index);

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project findByIndex(@NotNull String userId, @NotNull int index);

    @NotNull
    Project removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project startByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project startByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project finishById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project finishByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Project changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Project changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

}
