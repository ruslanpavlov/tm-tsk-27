package ru.tsc.pavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @Nullable
    List<Task> findTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    Task bindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    Task unbindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    Project removeById(@NotNull String userId, @Nullable String projectId);

    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    Project removeByName(@NotNull String userId, @Nullable String name);

}
