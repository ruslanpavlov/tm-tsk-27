package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyCommandException extends AbstractException {

    public EmptyCommandException(){
        super("Error: Command is empty.");
    }

}
