package ru.tsc.pavlov.tm.exception.entity;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class MutualNotFoundException extends AbstractException {

    public MutualNotFoundException(String task, String project) {
        super("Error. Task - Project " + task + " and " + project + "is wrong");
    }

}
