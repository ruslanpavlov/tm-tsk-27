package ru.tsc.pavlov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    private List<Project> projects;

    private List<Task> tasks;

    private List<User> users;

}
