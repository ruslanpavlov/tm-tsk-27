package ru.tsc.pavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.entity.IWBS;
import ru.tsc.pavlov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractOwnerEntity implements IWBS {

    public Project() {
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    private String name;

    private Status status = Status.NOT_STARTED;

    private String description;

    private Date startDate;

    private Date created = new Date();

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @NotNull
    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Nullable
    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

}
