package ru.tsc.pavlov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;
import com.jcabi.manifests.Manifests;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getString(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name))
            return System.getProperty(name);
        if (System.getenv().containsKey(name))
            return System.getenv(name);
        return properties.getProperty(name, defaultValue);
    }

    @Override
    public @NotNull String getPassSecret() {
        return getString(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public @NotNull Integer getPassIteration() {
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)) {
            final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getApplicationVersion() {
        return Manifests.read("version");
    }

    @Override
    public @NotNull String getDeveloperName() {
        return Manifests.read("developer");
    }

    @Override
    public @NotNull String getDeveloperEmail() {
        return Manifests.read("email");
    }
}
