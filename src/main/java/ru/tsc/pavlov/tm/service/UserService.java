package ru.tsc.pavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.IUserRepository;
import ru.tsc.pavlov.tm.api.service.IPropertyService;
import ru.tsc.pavlov.tm.api.service.IUserService;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserEmailException;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserLoginException;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserPasswordException;
import ru.tsc.pavlov.tm.exception.entity.UserNotFoundException;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.HashUtil;
import ru.tsc.pavlov.tm.util.StringUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final IUserRepository userRepository;

    @NotNull private final IPropertyService propertyService;


    public UserService(@NotNull final IUserRepository userRepository, @NotNull IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (StringUtil.isEmpty(login)) throw new EmptyUserLoginException();
        if (StringUtil.isEmpty(password)) throw new EmptyUserPasswordException(password);
        @NotNull final User user = new User(login, HashUtil.salt(propertyService,password), UserRole.USER);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login, @Nullable final String password, @Nullable final String email
    ) {
        if (StringUtil.isEmpty(login)) throw new EmptyUserLoginException();
        if (StringUtil.isEmpty(password)) throw new EmptyUserPasswordException(password);
        if (StringUtil.isEmpty(email)) throw new EmptyUserEmailException(login);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login, @Nullable final String password, @Nullable final UserRole role
    ) {
        if (StringUtil.isEmpty(login)) throw new EmptyUserLoginException();
        if (StringUtil.isEmpty(password)) throw new EmptyUserPasswordException(password);
        @NotNull final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    public void updateByLogin(
            @Nullable final String login, @Nullable final String lastName, @Nullable final String firstName,
            @Nullable final String middleName, @Nullable final String email
    ) {
        @NotNull final User user = userRepository.findByLogin(login);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
    }

    @Override
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyUserLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyUserLoginException();
        @NotNull final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }

    @Override
    public void setRole(@Nullable final String id, @Nullable final UserRole role) {
        @NotNull final User user = findById(id);
        user.setRole(role);
    }

    @Override
    public User findByLogin(@Nullable final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(@Nullable final String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        @NotNull final User user = findById(id);
        user.setPassword(HashUtil.salt(propertyService,password));
    }

    @Override
    public User removeByLogin(@Nullable final String login) {
        return userRepository.removeByLogin(login);
    }

}

