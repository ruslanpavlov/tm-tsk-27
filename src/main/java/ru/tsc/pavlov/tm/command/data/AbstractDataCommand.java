package ru.tsc.pavlov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.dto.Domain;
import ru.tsc.pavlov.tm.exception.entity.EntityNotFoundException;

public class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    @SneakyThrows
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) throw new EntityNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new EntityNotFoundException();
        serviceLocator.getUserService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void execute() {

    }

}
