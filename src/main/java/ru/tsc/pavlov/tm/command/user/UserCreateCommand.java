package ru.tsc.pavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import javax.management.relation.Role;

public class UserCreateCommand extends AbstractUserCommand {

    @Override
    public UserRole[] roles() {
        return new UserRole[]{UserRole.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.USER_CREATE_COMMAND;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Creating user";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER USER LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER USER MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSPHRASE:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER USER E-MAIL:");
        @NotNull final String email = TerminalUtil.nextLine();

        @Nullable final User user = getUserService().create(login, password, email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        showUser(user);
    }

}
